<?php 
/** 
 * Default template for AD Gallery enabled nodes
 * 
 * @see template_preprocess_ad_gallery_formatter_ad_gallery_default()
 */
?>

<?php if ($ad_gallery):?>

<div id="container">
	<div id="gallery" class="ad-gallery">
      	<div class="ad-image-wrapper"></div>
      	<div class="ad-controls"></div>
      	<div class="ad-nav">
        	<div class="ad-thumbs">
        		<ul class="ad-thumb-list">	
					<?php print $ad_gallery;?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php endif;?>    
 