Integration of AD Gallery (http://coffeescripter.com/code/ad-gallery/) and Drupal. 
You'll need the following modules to use this one:

- Imagefield
- Imagecache

Enable the module, add an image field to your selected node type making sure that your image 
field can handle multiple values, meaning that you're able to upload multiple images for a node 
(Number of values).

You can enable the Description and Title fields in the image field settings to add a description 
and a title to your image.
 
When done, go to the Display Settings tab and select AD Gallery for the 
display type. Your node's images should appear in an AD Gallery. 

The module is written with flexibility in mind, meaning that you're able to control the 
look and feel of your gallery and set up your preferred imagecache presets to use for the 
thumbnails and the main image. However, this means that you'll probably need to modify the 
attached CSS files here and there.

Adjust these to your settings:

The width of the whole gallery
.ad-gallery {
  width: 570px;
}

The main image wrapper
.ad-gallery .ad-image-wrapper {
    width: 100%;
    height: 300px;
    margin-bottom: 10px;
    position: relative;
    overflow: hidden;
}

The thumbnail area
.ad-gallery .ad-nav .ad-thumbs {
      overflow: hidden;
      width: 520px;
}