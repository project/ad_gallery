<?php 

/**
 *  Preprocess funcion for AD Gallery nodes 
 */

function template_preprocess_ad_gallery_formatter_ad_gallery_default(&$vars) {
	$field = $vars['element']['#field_name'];  
	$vars['node'] = node_load(arg(1));
	$children = element_children($vars['element']);
	foreach ($children as $key) {    
		if (!empty($vars['element'][$key]['#item']['fid'])) {
			$items[] = $vars['element'][$key]['#item'];
		}
	}	  
  
	$num_of_images = count($items);
	if($num_of_images > 0) {    
		$widget = ad_gallery_get_field_settings($field, $vars['element']['#type_name']);
		$vars['thumbs_preset'] = $widget['thumbnail'];
		$vars['main'] = $widget['main'];	    
	}	
  
	$vars['ad_gallery'] = '';  
	$gallery = node_load(arg(1));		
	$imagefield = $gallery->$field;
	
	foreach($imagefield as $key => $value){
		if ($value == ''){
			unset($imagefield[$key]);
		}
	}
	
	foreach($imagefield as $key => $value){									
		$picture = $value;			
		$thumbnail = theme('imagecache', $widget['thumbnail'], $picture['filepath'], $picture['data']['description'], $picture['data']['title'] );					
		$vars['ad_gallery'] .= '<li><a href="'. base_path() . file_directory_path() .'/imagecache/'. $widget['main'] .'/'.  $picture['filepath'] .'">'. $thumbnail .'</a></li>';			
	}     
}
